@extends('layout')

@section('content')
    @if($post->coverImage != null)
        <img class="img-fluid" src="/storage/coverImages/{{$post->coverImage}}" alt="{{$post->title}}" />
    @endif
    <div class="blog-post">
        <h2 class="blog-post-title">{{$post->title}}</h2>
        <p class="blog-post-meta">{{ $post->created_at }} by <a href="#">{{ $post->creator }}</a></p>
        {!! $post->body !!}
    </div>

    <a class="btn btn-secondary" href="/">Go back</a>
@endsection
