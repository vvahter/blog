@extends('layout')

@section('content')
    @if ($post)
        <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">{{$post->title}}</h1>
            <p class="lead my-3 promo-ellips">
                {{ $post->teaser }}
            </p>
            <p class="lead mb-0"><a href="/posts/{{$post->_id}}" class="text-white font-weight-bold">Continue reading...</a></p>
        </div>
    </div>
    @endif

    <posts></posts>
@endsection
