@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <a class="btn btn-primary" href="/posts/create">Create a post</a>
            <h2 class="mt-3">Blog posts</h2>
            <admin-posts class="mt-3"></admin-posts>
        </div>
    </div>
</div>
@endsection
