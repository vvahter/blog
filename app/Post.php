<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Blog post.
 */
class Post extends Eloquent
{
    protected $connection = 'mongodb';
    protected $dates = ['created_at', 'updated_at'];
}
