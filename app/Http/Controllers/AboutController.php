<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * About me.
 */
class AboutController extends Controller
{
    public function index() {
        return view('about');
    }
}
