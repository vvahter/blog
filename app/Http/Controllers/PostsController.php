<?php

namespace App\Http\Controllers;

use App\Post;
use App\Services\PostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Manage blog posts.
 */
class PostsController extends Controller
{
    /**
     * @var PostService
     */
    private $postService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostService $postService)
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->postService = $postService;
    }

    /**
     * Get post paginated and as json.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')
            ->paginate(6);

        return response()->json($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'teaser' => 'required',
            'coverImage' => 'image|nullable|max:1999'
        ]);

        $hasFile = $request->hasFile('coverImage');
        $fileNameToStore = null;

        // File upload
        if ($hasFile) {
            $file = $request->file('coverImage');
            $fileNameToStore = $this->postService->savePostFile($file);
        }

        $postId = $this->postService->createPost(
            $request->input('title'),
            $request->input('body'),
            $request->input('teaser'),
            $fileNameToStore
        );

        return response()->json([
            'id' => $postId,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit')->with('post', $post->toJson());
    }

    /**
     * Update the specified resource in storage.
     * NB! Can't use PUT method because: https://github.com/laravel/framework/issues/13457
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'teaser' => 'required',
            'coverImage' => 'image|nullable|max:1999'
        ]);

        $hasFile = $request->hasFile('coverImage');
        $fileNameToStore = null;

        // File upload
        if ($hasFile) {
            $file = $request->file('coverImage');
            $fileNameToStore = $this->postService->savePostFile($file);
        }

        $postId = $this->postService->updatePost(
            $id,
            $request->input('title'),
            $request->input('body'),
            $request->input('teaser'),
            $fileNameToStore
        );

        return response()->json([
            'id' => $postId,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->postService->deletePost($id);

        return response()->json();
    }
}
