<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

/**
 * Startpage
 */
class IndexController extends Controller
{
    /**
     * Get the latest post to promo.
     * Other posts are loaded dynamically.
     */
    public function index() {
        $latestPost = Post::orderBy('created_at', 'desc')->first();

        return view('index')->with('post', $latestPost);
    }
}
