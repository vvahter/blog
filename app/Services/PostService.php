<?php

namespace App\Services;

use App\Post;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * @todo Add unittests.
 */
class PostService
{
    /**
     * Create new post.
     *
     * @param string $title      Post title
     * @param string $body       Post body
     * @param string $teaser     Post teaser
     * @param string $coverImage Post image
     *
     * @return string
     */
    public function createPost(string $title, string $body, string $teaser, ?string $coverImage): string {
        $post = new Post();
        $post = $this->fillPostFields($post, $title, $body, $teaser, $coverImage);

        $post->created_at = new \DateTime();
        $post->creator = auth()->user()->name;
        $post->save();

        return $post->id;
    }

    /**
     * Save post file.
     *
     * @param UploadedFile $file Uploaded file.
     *
     * @return string File name.
     */
    public function savePostFile(UploadedFile $file): string {
        $fileNameWithExt = $file->getClientOriginalName();
        $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        $extension = $file->getClientOriginalExtension();
        $fileNameToStore = $fileName.'_'.time().'.'.$extension;
        $path = $file->storeAs('public/coverImages', $fileNameToStore);

        return $fileNameToStore;
    }

    /**
     * Update existing post.
     *
     * @param string $postId     Post id
     * @param string $title      Post title
     * @param string $body       Post body
     * @param string $teaser     Post teaser
     * @param string $coverImage Post image
     *
     * @return string
     */
    public function updatePost(string $postId, string $title, string $body, string $teaser, ?string $coverImage): string {

        $post = Post::find($postId);
        $prevImage = $post->coverImage;

        $post = $this->fillPostFields($post, $title, $body, $teaser, $coverImage);
        $post->save();

        if ($prevImage && $coverImage) {
            Storage::delete('public/coverImages/'.$prevImage);
        }

        return $post->id;
    }

    /**
     * Delete post.
     *
     * @param  string $postId Post id
     */
    public function deletePost(string $postId) {
        $post = Post::find($postId);

        if ($post->coverImage) {
            Storage::delete('public/coverImages/'.$post->coverImage);
        }

        $post->delete();
    }

    /**
     * Fill post fields.
     *
     * @param Post   $post       Post object
     * @param string $title      Post title
     * @param string $body       Post body
     * @param string $teaser     Post teaser
     * @param string $coverImage Post image
     *
     * @return Post
     */
    private function fillPostFields(Post $post, string $title, string $body, string $teaser, ?string $coverImage): Post {
        $post->title = $title;
        $post->body = $body;
        $post->teaser = $teaser;
        $post->updated_at = new \DateTime();

        if ($coverImage !== null) {
            $post->coverImage = $coverImage;
        }

        return $post;
    }
}