<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routse for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/about', 'AboutController@index');

Route::resource('posts', 'PostsController');
Route::post('/posts/{post}', 'PostsController@update')->name('posts.update');
Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
