## BLOG

Laravel 5.8.14
PHP 7.2

**1. Copy .env.example and fill MYSQL and MONGODB info.**

**2. Run composer install**
```
$ composer install
```

**3. Create MYSQL DB**
   ```
   CREATE DATABASE blog;
   ```

**4. Migrate**
```
php artisan migrate
```

**5. Seed**
```
php artisan db:seed

```

**6. Create MONGODB database blog**

**7. Add MONGODB user**
```
db.createUser({user: 'blog', pwd: 'blog_password', roles:[{role:'readWrite',db:'blog'}]});
```

**8. Add storage link**
```
php artisan storage:link
```

**9. Run npm install**
```
$ npm install
```

**10. Build assets**
```
$ npm run dev
```

**11. Start web server**
```
$ php artisan serve
```

**Logins**
Admin username: admin@blog.com
Admin password: password


## Still todo/add in future
1. Add unittests (at least for PostService)
2. Make better UI
3. Add posts history per months (months widgets)
4. Subscription button
5. Comments